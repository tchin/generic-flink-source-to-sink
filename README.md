# Generic Flink Source to Sink

An experiment to abstract away all source and sink code into config variables
so that all a user would have to do is write the pipeline transformation.

## Configuration
All configuration options with their default values for local development can be seen in `config.properties`.

You can set these config variables in three ways:
- Environment variables
  - Command line arguments
    - Example Yarn job submission:
  ```bash
  ./bin/flink run -c org.wikimedia.dataplatform.WikiKafkaToPrintPipeline \
  generic-flink-source-to-sink-1.0-SNAPSHOT-jar-with-dependencies.jar \
  --FLINK_SOURCE_NAME image_suggestions_feedback \
  --EVENT_SCHEMA_BASE_URIS https://schema.wikimedia.org/repositories/primary/jsonschema,https://schema.wikimedia.org/repositories/secondary/jsonschema \
  --EVENT_STREAM_CONFIG_URI https://meta.wikimedia.org/w/api.php \
  --KAFKA_STREAM_NAME mediawiki.image_suggestions_feedback \
  --KAFKA_BOOTSTRAP_SERVERS kafka-jumbo1001.eqiad.wmnet:9092 \
  --KAFKA_GROUP_ID image_suggestions_feedback_group
  ```
- Properties file
  - To use this, set the `PROPERTIES_FILE` config option
  to the file path of the properties file
  - When doing local dev in IntelliJ, setting `--PROPERTIES_FILE config.properties`
  in the run configurations uses the included config file

If the same property is set multiple times,
the value is overwritten with this precedence:
env variables > cli args > config file.

There are no default values for unset properties.

Below are list of config variables currently used by various sinks (warning: outdated)
<details>
<summary>Click here to see a list of all properties</summary>

| Property                  | Required?        | Additional Info                                                                                                                                                                                                                                                         |
|---------------------------|------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `FLINK_SOURCE_NAME`       | Yes              |                                                                                                                                                                                                                                                                         |
| `KAFKA_STREAM_NAME`       | Yes              |                                                                                                                                                                                                                                                                         |
| `KAFKA_BOOTSTRAP_SERVERS` | Yes              |                                                                                                                                                                                                                                                                         |
| `KAFKA_GROUP_ID`          | Yes              |                                                                                                                                                                                                                                                                         |
| `EVENT_SCHEMA_BASE_URIS`  | Yes              | Comma-seperated list.                                                                                                                                                                                                                                                   |
| `EVENT_STREAM_CONFIG_URI` | Yes              |                                                                                                                                                                                                                                                                         |
| `KAFKA_SOURCE_ROUTES`     | No               | Comma-seperated list. Used to proxy requests. Maps to `KAFKA_CLIENT_ROUTES`.                                                                                                                                                                                            |
| `KAFKA_CLIENT_ROUTES`     | No               | Comma-seperated list. Used to proxy requests. Maps to `KAFKA_SOURCE_ROUTES`.                                                                                                                                                                                            |
| `FIELD_NAMES`             | Yes              | Case-and-order-sensitive comma-seperated list. Maps to the column names of the sink table. Values are pulled from the fields of the source's JSON messages. If a field isn't present, you must manually generate it through the Flink pipeline before it gets inserted. |
| `CASSANDRA_HOST`          | `SINK=CASSANDRA` |                                                                                                                                                                                                                                                                         |
| `CASSANDRA_PORT`          | `SINK=CASSANDRA` |                                                                                                                                                                                                                                                                         |
| `CASSANDRA_USERNAME`      | `SINK=CASSANDRA` |                                                                                                                                                                                                                                                                         |
| `CASSANDRA_PASSWORD`      | `SINK=CASSANDRA` |                                                                                                                                                                                                                                                                         |
| `CASSANDRA_TABLE`         | `SINK=CASSANDRA` |                                                                                                                                                                                                                                                                         |
| `JDBC_DRIVER`             | `SINK=JDBC`      | Must have the required JDBC driver installed.                                                                                                                                                                                                                           |
| `JDBC_URL`                | `SINK=JDBC`      |                                                                                                                                                                                                                                                                         |
| `JDBC_USERNAME`           | `SINK=JDBC`      |                                                                                                                                                                                                                                                                         |
| `JDBC_PASSWORD`           | `SINK=JDBC`      |                                                                                                                                                                                                                                                                         |
| `JDBC_TABLE`              | `SINK=JDBC`      |                                                                                                                                                                                                                                                                         |
| `JDBC_FIELD_TYPES`        | `SINK=JDBC`      | Naps to `FIELD_NAMES`. Supported values are `VARCHAR`, `INT`, `BOOL`, and `UUID`. `UUID` is used to indicate a `UUID` type should be converted to a string before insertion.                                                                                            |
| `JDBC_BATCH_SIZE`         | `SINK=JDBC`      |                                                                                                                                                                                                                                                                         |
| `JDBC_BATCH_INTERVAL_MS`  | `SINK=JDBC`      |                                                                                                                                                                                                                                                                         |
| `JDBC_MAX_RETRY`          | `SINK=JDBC`      |                                                                                                                                                                                                                                                                         |

</details>

## Local Development
A few files have been provided to aid in local development.
- `docker-compose.yml`
spins up a local dev environment with Kafka and Cassandra using
`docker-compose up`
- `setup-docker-env.sh`
creates the topics and databases in Kafka and Cassandra,
assuming Cassandra has the default username and password.
Run this after spinning up the Docker containers
- `image-suggestions-feedback.cql`
contains the CQL run on Cassandra by `setup-docker-env.sh`
- `cassandra.yaml`
contains the config used by the Cassandra container
- `enter.sh`
is a helper script to easily access the Docker containers.
It is currently hardcoded with default values
  - `./enter.sh consumer [equid/codfw]` to enter the equid/codfw Kafka consumer
  - `./enter.sh producer [equid/codfw]` to enter the equid/codfw Kafka producer
  - `./enter.sh cassandra` to ender Cassandra's cqlsh
- Any persistent data from Docker containers is stored in
`\cache\docker`



## Test and build

Test with
```bash
./mvnw clean verify
```

Build a jar with dependencies with
```bash
./mvnw clean package
```
