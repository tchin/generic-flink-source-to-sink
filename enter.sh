case "$1" in
    "consumer") docker exec -it flink-broker bash /bin/kafka-console-consumer --bootstrap-server localhost:9092 --topic "$2".mediawiki.image_suggestions_feedback --from-beginning;;
    "producer") docker exec -it flink-broker bash /bin/kafka-console-producer --topic "$2".mediawiki.image_suggestions_feedback --broker-list localhost:9092;;
    "cassandra") docker exec -it flink-cassandra cqlsh -u cassandra -p cassandra;;
esac
