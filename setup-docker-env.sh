#docker-compose up

#docker exec -it flink-broker /bin/kafka-topics --bootstrap-server localhost:9092 --topic image_suggestions_feedback --create
docker exec -it flink-broker /bin/kafka-topics --bootstrap-server localhost:9092 --topic eqiad.mediawiki.image_suggestions_feedback --create
docker exec -it flink-broker /bin/kafka-topics --bootstrap-server localhost:9092 --topic codfw.mediawiki.image_suggestions_feedback --create
docker exec -it flink-cassandra cqlsh -u cassandra -p cassandra -f /image-suggestions-feedback.cql