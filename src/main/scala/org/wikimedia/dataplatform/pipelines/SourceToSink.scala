package org.wikimedia.dataplatform.pipelines

import io.findify.flink.api.StreamExecutionEnvironment
import org.apache.flink.api.java.utils.ParameterTool
import org.wikimedia.dataplatform.sources.Sourcable
import org.wikimedia.dataplatform.sinks.Sinkable


object SourceToSink {
  def getParameters(args: Array[String]): ParameterTool = {
    val envParams = ParameterTool.fromMap(System.getenv())
    val argsParams = ParameterTool.fromArgs(args)
    val mergedParams = argsParams.mergeWith(envParams)
    val propertiesFile = Option(mergedParams.get("PROPERTIES_FILE"))

    propertiesFile match {
      case None => mergedParams
      case Some(value) => ParameterTool.fromPropertiesFile(value)
        .mergeWith(argsParams)
        .mergeWith(envParams)
    }
  }
}

/**
 *
 * @param source
 * @param sink
 * @tparam T Type of DataStream created by source
 * @tparam R Type of DataStream sunk into sink
 */
abstract class SourceToSink[T, R](source: Sourcable[T], sink: Sinkable[R]) extends Transformable[T, R] {
  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

  def execute(args: Array[String]): Unit = {
    implicit val parameters: ParameterTool = SourceToSink.getParameters(args)
    env.getConfig.setGlobalJobParameters(
      parameters
    )

    val stream = source.sourceFrom(env)
    sink.sinkTo(transform(stream))

    env.execute()
  }

  protected def main(args: Array[String]): Unit = {
    execute(args)
  }
}
