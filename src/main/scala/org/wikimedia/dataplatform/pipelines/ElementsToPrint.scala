package org.wikimedia.dataplatform.pipelines

import io.findify.flink.api.DataStream
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.wikimedia.dataplatform.sinks.PrintSink
import org.wikimedia.dataplatform.sources.ElementsSource

class ElementsToPrint[T](typeInfo: TypeInformation[T], elements: T*)
  extends SourceToSink(
    new ElementsSource[T](typeInfo, elements: _*),
    new PrintSink[T]
  ) {

  override def transform(stream: DataStream[T]): DataStream[T] = {
    stream
  }
}
