package org.wikimedia.dataplatform.pipelines

import io.findify.flink.api.DataStream

/**
 *
 * @tparam T Type of input DataStream
 * @tparam R Type of output DataStream
 */
trait Transformable[T, R] {
  def transform(stream: DataStream[T]): DataStream[R]
}
