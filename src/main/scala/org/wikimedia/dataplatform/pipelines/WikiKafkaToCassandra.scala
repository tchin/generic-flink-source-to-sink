package org.wikimedia.dataplatform.pipelines

import org.apache.flink.api.java.typeutils.RowTypeInfo
import org.wikimedia.dataplatform.sinks.CassandraRowSink
import org.wikimedia.dataplatform.sources.WikiKafkaSource


abstract class WikiKafkaToCassandra(cassandraRowTypeInfo: RowTypeInfo)
  extends SourceToSink(new WikiKafkaSource, new CassandraRowSink(cassandraRowTypeInfo))
