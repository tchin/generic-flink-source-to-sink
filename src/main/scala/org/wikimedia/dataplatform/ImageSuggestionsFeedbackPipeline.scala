package org.wikimedia.dataplatform

import com.datastax.driver.core.utils.UUIDs
import io.findify.flink.api._
import io.findify.flinkadt.api._
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.typeutils.RowTypeInfo
import org.apache.flink.types.{Row, RowKind, RowUtils}
import org.wikimedia.dataplatform.pipelines.WikiKafkaToCassandra

import java.util.UUID

object cassandraRowTypeInfo {
  val typeInfo: RowTypeInfo = new RowTypeInfo(
    Array[TypeInformation[_]](
      stringInfo, intInfo, stringInfo, TypeInformation.of(classOf[UUID]),
      stringInfo, intInfo, boolInfo, boolInfo, stringInfo
    ),
    Array[String](
      "wiki", "page_id", "filename", "id",
      "origin_wiki", "user_id", "is_accepted", "is_rejected", "rejection_reason"
    )
  )
}

/** An implementation of the image suggestions feedback pipeline complete with typeinfo
 */
object ImageSuggestionsFeedbackPipeline extends WikiKafkaToCassandra(cassandraRowTypeInfo.typeInfo) {
  override def transform(stream: DataStream[Row]): DataStream[Row] = {
    val sourceRowTypeInfo: RowTypeInfo = stream.dataType.asInstanceOf[RowTypeInfo]
    val sinkRowTypeInfo = cassandraRowTypeInfo.typeInfo

    stream
      .filter(row => Option(row.getFieldAs[Row]("meta").getFieldAs[String]("domain")) match {
        case Some(value) => value != "canary"
        case _ => true
      })
      .map(row => {
        // The row received is a hybrid row, which doesn't allow addition of new fields
        // so we map the fields we need into a field-based row
        // NOTE: Row.project does not work on hybrid rows if there's an unknown column name
        // which in this case is the id we want to add
        val fieldRow = Row.withNames()
        val fieldNames = sinkRowTypeInfo.getFieldNames
        for (name <- fieldNames) {
          if (sourceRowTypeInfo.hasField(name)) {
            fieldRow.setField(name, row.getField(name))
          }
        }

//        val namedPositionRow = RowUtils.createRowWithNamedPositions(RowKind.INSERT, new Object[], fieldByPositions)

        fieldRow.setField("id", UUIDs.timeBased())

        // (Currently) the value for `filename` is a full url, so we fetch the filename
        fieldRow.setField("filename", row.getFieldAs[String]("filename").split(":").last)
        // Numbers are by default type Long, which cannot be inserted into Cassandra int columns
        fieldRow.setField("page_id", Math.toIntExact(row.getFieldAs[Long]("page_id")))
        fieldRow.setField("user_id", Math.toIntExact(row.getFieldAs[Long]("user_id")))

        // Filter and reorder required columns
        val filteredFieldRow = Row.project(fieldRow, fieldNames)

        // Convert filteredFieldRow into a position-based row which is insertable into Cassandra
        val positionRow = Row.withPositions(filteredFieldRow.getArity)
        fieldNames.zipWithIndex.foreach {
          case (field, idx) => positionRow.setField(idx, filteredFieldRow.getField(field))
        }
        positionRow
      }
    )(sinkRowTypeInfo)
  }
}
