package org.wikimedia.dataplatform.sources

import io.findify.flink.api.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.api.java.utils.ParameterTool

/**
 *
 * @tparam T Type of DataStream that will be created from the source
 */
trait Sourcable[T] {
  def sourceFrom(environment: StreamExecutionEnvironment)(implicit parameters: ParameterTool): DataStream[T]
}
