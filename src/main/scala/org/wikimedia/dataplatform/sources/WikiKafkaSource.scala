package org.wikimedia.dataplatform.sources

import io.findify.flink.api.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.api.common.eventtime.WatermarkStrategy
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.connector.kafka.source.KafkaSource
import org.apache.flink.types.Row
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory

import scala.collection.JavaConverters._

class WikiKafkaSource extends Sourcable[Row] {

  override def sourceFrom(environment: StreamExecutionEnvironment)(implicit parameters: ParameterTool): DataStream[Row] = {
    val kafkaStreamName = parameters.getRequired("KAFKA_STREAM_NAME")
    val (kafkaSource, eventDataStreamFactory) = buildKafkaWikiSource
    environment.fromSource(
      kafkaSource,
      WatermarkStrategy.noWatermarks(),
      parameters.getRequired("FLINK_SOURCE_NAME")
    )(eventDataStreamFactory.rowTypeInfo(kafkaStreamName))
  }

  private def buildKafkaWikiSource(implicit parameters: ParameterTool): (KafkaSource[Row], EventDataStreamFactory) = {
    val kafkaBootstrapServers = parameters.getRequired("KAFKA_BOOTSTRAP_SERVERS")
    val kafkaGroupId = parameters.getRequired("KAFKA_GROUP_ID")
    val kafkaStreamName = parameters.getRequired("KAFKA_STREAM_NAME")

    val kafkaSourceRoutes = Option(parameters.get("KAFKA_SOURCE_ROUTES"))
    val kafkaClientRoutes = Option(parameters.get("KAFKA_CLIENT_ROUTES"))

    val eventSchemaBaseUris = parameters.getRequired("EVENT_SCHEMA_BASE_URIS").split(",")
    val eventStreamConfigUri = parameters.getRequired("EVENT_STREAM_CONFIG_URI")
    val kafkaHttpClientRoutes = (kafkaSourceRoutes, kafkaClientRoutes) match {
      case (Some(source), Some(client)) =>
        Option(
          source.split(",")
            .zip(client.split(","))
            .toMap.asJava
        )
      case _ => None
    }

    val factory = EventDataStreamFactory.from(
      eventSchemaBaseUris.toList.asJava,
      eventStreamConfigUri,
      kafkaHttpClientRoutes.orNull
    )
    val kafkaSource = factory.kafkaSourceBuilder(
      kafkaStreamName,
      kafkaBootstrapServers,
      kafkaGroupId
    )
      .build()

    (kafkaSource, factory)
  }
}
