package org.wikimedia.dataplatform.sources

import io.findify.flink.api.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.utils.ParameterTool

class ElementsSource[T](typeInformation: TypeInformation[T], elements: T*) extends Sourcable[T] {
  override def sourceFrom(environment: StreamExecutionEnvironment)(implicit parameters: ParameterTool): DataStream[T] = {
    environment.fromElements(elements: _*)(typeInformation)
  }
}
