package org.wikimedia.dataplatform.sinks

import io.findify.flink.api.DataStream
import org.apache.flink.api.java.utils.ParameterTool

class PrintSink[T] extends Sinkable[T] {
  override def sinkTo(stream: DataStream[T])(implicit parameters: ParameterTool): Unit = {
    stream.print()
  }
}
