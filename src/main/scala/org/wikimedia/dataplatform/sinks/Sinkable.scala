package org.wikimedia.dataplatform.sinks

import io.findify.flink.api.DataStream
import org.apache.flink.api.java.utils.ParameterTool

/**
 *
 * @tparam T Type of DataStream that is being sinked
 */
trait Sinkable[T] {
  def sinkTo(stream: DataStream[T])(implicit parameters: ParameterTool): Unit
}
