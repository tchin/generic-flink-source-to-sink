package org.wikimedia.dataplatform.sinks

import io.findify.flink.api.DataStream
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.connector.jdbc.{JdbcConnectionOptions, JdbcExecutionOptions, JdbcSink, JdbcStatementBuilder}
import org.apache.flink.streaming.api.functions.sink.SinkFunction

abstract class JdbcSink[T] extends Sinkable[T] {
  val jdbcStatementBuilder: JdbcStatementBuilder[T]

  override def sinkTo(stream: DataStream[T])(implicit parameters: ParameterTool): Unit = {
    stream.addSink(jdbcSink)
  }

  private def jdbcSink(implicit parameters: ParameterTool): SinkFunction[T] = { // scalastyle:ignore
    val fieldNames = parameters.getRequired("FIELD_NAMES").split(",")

    val jdbcUrl = parameters.getRequired("JDBC_URL")
    val jdbcDriver = parameters.getRequired("JDBC_DRIVER")
    val jdbcUser = parameters.getRequired("JDBC_USERNAME")
    val jdbcPass = parameters.getRequired("JDBC_PASSWORD")
    val jdbcTable = parameters.getRequired("JDBC_TABLE")

    val jdbcBatchSize = parameters.getRequired("JDBC_BATCH_SIZE").toInt
    val jdbcBatchIntervalMs = parameters.getRequired("JDBC_BATCH_INTERVAL_MS").toInt
    val jdbcMaxRetry = parameters.getRequired("JDBC_MAX_RETRY").toInt

    val jdbcQuery =
      s"""INSERT INTO $jdbcTable(${fieldNames.mkString(",")})
         |values(${fieldNames.map(_ => "?").mkString(",")})
         |""".stripMargin

    JdbcSink.sink[T](
      jdbcQuery,
      jdbcStatementBuilder,
      JdbcExecutionOptions.builder()
        .withBatchSize(jdbcBatchSize)
        .withBatchIntervalMs(jdbcBatchIntervalMs)
        .withMaxRetries(jdbcMaxRetry)
        .build(),
      new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
        .withUrl(jdbcUrl)
        .withDriverName(jdbcDriver)
        .withUsername(jdbcUser)
        .withPassword(jdbcPass)
        .build()
    )
  }
}