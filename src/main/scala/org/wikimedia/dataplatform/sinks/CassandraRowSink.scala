package org.wikimedia.dataplatform.sinks

import com.datastax.driver.core.Cluster
import io.findify.flink.api.DataStream
import org.apache.flink.api.java.typeutils.RowTypeInfo
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.connectors.cassandra.CassandraSink.CassandraRowSinkBuilder
import org.apache.flink.streaming.connectors.cassandra.ClusterBuilder
import org.apache.flink.types.Row

class CassandraRowSink(typeInfo: RowTypeInfo) extends Sinkable[Row] {
  override def sinkTo(stream: DataStream[Row])(implicit parameters: ParameterTool): Unit = {
    sinkToCassandra(stream)
  }

  private def sinkToCassandra(dataStream: DataStream[Row])(implicit parameters: ParameterTool): Unit = {
    val cassandraHost = parameters.getRequired("CASSANDRA_HOST")
    val cassandraPort = parameters.getRequired("CASSANDRA_PORT").toInt
    val cassandraUser = parameters.getRequired("CASSANDRA_USERNAME")
    val cassandraPass = parameters.getRequired("CASSANDRA_PASSWORD")

    val cassandraTable = parameters.getRequired("CASSANDRA_TABLE")
    val fieldNames = typeInfo.getFieldNames
    val cassandraQuery = s"""INSERT INTO $cassandraTable(${fieldNames.mkString(",")})
       | values(${fieldNames.map(_ => "?").mkString(",")})
       | """.stripMargin

    new CassandraRowSinkBuilder(
      dataStream.javaStream,
      typeInfo,
      typeInfo.createSerializer(
        dataStream.javaStream.getExecutionEnvironment.getConfig
      )
    )
      .setQuery(cassandraQuery)
      .setClusterBuilder(
        new ClusterBuilder() {
          def buildCluster(builder: Cluster.Builder): Cluster = {
            builder
              .addContactPoint(cassandraHost)
              .withPort(cassandraPort)
              .withCredentials(cassandraUser, cassandraPass)
              .build
          }
        }
      )
      .build()
  }
}
