package org.wikimedia.dataplatform

import org.wikimedia.dataplatform.pipelines.ElementsToPrint

import io.findify.flinkadt.api._
import scala.util.Random

/** Example pipeline
 */
object PrintRandomNumbersPipeline {
  def main(args: Array[String]): Unit = {
    val random = Seq.fill(10)(Random.nextInt)
    val pipeline = new ElementsToPrint[Int](intInfo, random: _*)

    pipeline.env.setParallelism(1)
    pipeline.execute(args)
  }
}
