package org.wikimedia.dataplatform

import io.findify.flink.api._
import io.findify.flinkadt.api._
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.types.Row
import org.wikimedia.dataplatform.pipelines.SourceToSink
import org.wikimedia.dataplatform.sinks.PrintSink
import org.wikimedia.dataplatform.sources.WikiKafkaSource

abstract class WikiKafkaToPrint
  extends SourceToSink[Row, Row](new WikiKafkaSource, new PrintSink)

/** A generic pipeline that prints out rows from a Kafka source
 *
 *  You can specify fields to be printed with a comma-seperated list in the config variable FIELD_NAMES
 */
object WikiKafkaToPrintPipeline {
  def main(args: Array[String]): Unit = {
    val parameters: ParameterTool = SourceToSink.getParameters(args)
    val fieldNames = Option(parameters.get("FIELD_NAMES"))

    val pipeline = new WikiKafkaToPrint {
      override def transform(stream: DataStream[Row]): DataStream[Row] = {
        // Move fieldNames into scope so transform can be serialized
        val _fieldNames = fieldNames
        stream.map(row => {
          // Pluck and reorder required fields
          _fieldNames match {
            case Some(fields) => Row.project(row, fields.split(","))
            case None => row
          }
        })(TypeInformation.of(classOf[Row]))
      }
    }

    pipeline.execute(args)
  }
}
