package org.wikimedia.dataplatform

import io.findify.flinkadt.api._
import org.apache.flink.api.java.utils.ParameterTool
import org.wikimedia.dataplatform.pipelines.{ElementsToPrint, SourceToSink}

import scala.collection.convert.ImplicitConversions._

/** Example pipeline
 */
object PrintConfigPipeline {
  def main(args: Array[String]): Unit = {
    val parameters: ParameterTool = SourceToSink.getParameters(args)
    val pipeline = new ElementsToPrint[String](stringInfo, parameters.toMap.values().toSeq: _*)

    pipeline.env.setParallelism(1)
    pipeline.execute(args)
  }
}
