package org.wikimedia.dataplatform

import io.findify.flink.api._
import io.findify.flinkadt.api._
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.typeutils.RowTypeInfo
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.types.Row
import org.wikimedia.dataplatform.pipelines.{SourceToSink, WikiKafkaToCassandra}

/** A generic pipeline that takes a Kafka source, plucks specific fields from its messages,
 *  and inserts it into Cassandra.
 *
 *  The fields plucked should be defined as a comma-seperated list in the config variable FIELD_NAMES
 */
object WikiKafkaToCassandraPipeline {
  def main(args: Array[String]): Unit = {
    val parameters: ParameterTool = SourceToSink.getParameters(args)
    val fieldNames = parameters.getRequired("FIELD_NAMES").split(",")
    val rowTypeInfo = new RowTypeInfo(fieldNames.map(_ => unitInfo): Array[TypeInformation[_]], fieldNames)

    val pipeline = new WikiKafkaToCassandra(rowTypeInfo) {
      override def transform(stream: DataStream[Row]): DataStream[Row] = {
        // Move fieldNames into scope so transform can be serialized
        val _fieldNames = fieldNames
        stream.map(row => {
          // Pluck and reorder required fields
          val filteredFieldRow = Row.project(row, _fieldNames)

          // Convert filteredFieldRow into a position-based row which is insertable into Cassandra
          val positionRow = Row.withPositions(filteredFieldRow.getArity)
          _fieldNames.zipWithIndex.foreach {
            case (field, idx) => positionRow.setField(idx, filteredFieldRow.getField(field))
          }
          positionRow
        })(rowTypeInfo)
      }
    }

    pipeline.execute(args)
  }
}
