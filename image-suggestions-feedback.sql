-- NOTE: Everything here is arbitrary and only used for testing the JDBC connection
CREATE TABLE IF NOT EXISTS image_suggestions_feedback(
    wiki             varchar(16),
    page_id          int,
    filename         varchar(128),
    id               varchar(36) primary key,
    origin_wiki      varchar(16),
    user_id          int,
    is_accepted      boolean,
    is_rejected      boolean,
    rejection_reason varchar(1024)
);